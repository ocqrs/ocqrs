package wraperr

import "fmt"

func Wrap(err error, msg string) error {
	return fmt.Errorf(msg+": %w", err)
}
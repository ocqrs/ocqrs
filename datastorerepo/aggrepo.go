package datastorerepo

import (
	"cloud.google.com/go/datastore"
	"context"
	"encoding/json"
	"errors"
	"github.com/ozouai/ocqrs"
	"github.com/ozouai/ocqrs/wraperr"
)

type Repo struct {
	client *datastore.Client
	appKey *datastore.Key

	eventFactories map[string]map[string]func(b *ocqrs.EventBase) ocqrs.Event
}

type eventRecord struct {
	Type string
	Data interface{}

	repo *Repo
	dataCache []datastore.Property
}

func NewRepo(gcpProject string, appName string, aggs []ocqrs.Aggregate) (*Repo, error) {
	c, err := datastore.NewClient(context.TODO(), gcpProject)
	if err != nil {
		return nil, wraperr.Wrap(err, "error creating client")
	}
	r := &Repo{client: c, appKey: datastore.NameKey("app", appName, nil), eventFactories: map[string]map[string]func(b *ocqrs.EventBase) ocqrs.Event{}}
	for _, a := range aggs {
		r.eventFactories[ocqrs.TypeOf(a)] = map[string]func(b *ocqrs.EventBase) ocqrs.Event{}
		for _, f := range a.EventFactories() {
			r.eventFactories[ocqrs.TypeOf(a)][ocqrs.TypeOf(f(nil))] = f
		}
	}
	return r, nil
}

func (m *Repo) AggExists(ctx context.Context, agg ocqrs.Aggregate) (bool, error) {
	tx := txFromCtx(ctx)
	key := datastore.NameKey(ocqrs.TypeOf(agg), agg.ID().ID, m.appKey)
	q := datastore.NewQuery(ocqrs.TypeOf(agg)).Filter("__key__=", key).KeysOnly().Transaction(tx)
	count, err := m.client.Count(ctx, q)
	if err != nil {
		return false, wraperr.Wrap(err, "error checking exists")
	}
	return count != 0, nil
}

type keyT string

var ctxKey = keyT("datastore")

func txFromCtx(ctx context.Context) *datastore.Transaction {
	val := ctx.Value(ctxKey)
	if val == nil {
		return nil
	}
	tx, ok := val.(*datastore.Transaction)
	if !ok {
		return nil
	}
	return tx
}

func (m *Repo) BeginTx(ctx context.Context) (context.Context, error) {
	tx, err := m.client.NewTransaction(ctx)
	if err != nil {
		return nil, wraperr.Wrap(err, "error starting tx")
	}
	subCtx := context.WithValue(ctx, ctxKey, tx)
	return subCtx, nil
}

func (m *Repo) CommitTx(ctx context.Context) (error) {
	tx := txFromCtx(ctx)
	if tx == nil {
		return errors.New("error no tx")
	}
	_, err := tx.Commit()
	if err != nil {
		return wraperr.Wrap(err, "error committing")
	}
	return nil
}

func (m *Repo) RollbackTx(ctx context.Context) error {
	tx := txFromCtx(ctx)
	if tx == nil {
		return errors.New("error no tx")
	}
	err := tx.Rollback()
	if err != nil {
		return wraperr.Wrap(err, "error rolling back")
	}
	return nil
}

func (m *Repo) WithTx(ctx context.Context, f func(ctx context.Context) error) error {
	subCtx, err := m.BeginTx(ctx)
	if err != nil {
		return wraperr.Wrap(err, "error starting tx")
	}
	err = f(subCtx)
	if err != nil {
		subErr := m.RollbackTx(subCtx)
		if subErr != nil {
			// TODO log
		}
		return wraperr.Wrap(err, "error in f")
	}
	err = m.CommitTx(subCtx)
	if err != nil {
		return wraperr.Wrap(err, "error commiting tx")
	}
	return nil
}

func (m *Repo) FetchAgg(ctx context.Context, agg ocqrs.Aggregate) error {
	key := datastore.NameKey(ocqrs.TypeOf(agg), agg.ID().ID, m.appKey)
	tx := txFromCtx(ctx)
	q := datastore.NewQuery("event").Transaction(tx).Ancestor(key)
	output := []*eventRecord{}
	_, err := m.client.GetAll(ctx, q, &output)
	if err != nil {
		return wraperr.Wrap(err, "error getting events")
	}
	for _, e := range output {
		knownAgg, ok := m.eventFactories[ocqrs.TypeOf(agg)]
		if !ok {
			return errors.New("unknown agg")
		}
		knownEvent, ok := knownAgg[e.Type]
		if !ok {
			return errors.New("unknown type")
		}
		ev := knownEvent(ocqrs.NewEventBase(agg))
		err = datastore.LoadStruct(ev, e.dataCache)
		if err != nil {
			return wraperr.Wrap(err, "error loading struct")
		}
		agg.Apply(ev)
		agg.IncrementVersion()
	}

	return nil
}

func (m *Repo) CreateAgg(ctx context.Context, agg ocqrs.Aggregate) error {
	key := datastore.NameKey(ocqrs.TypeOf(agg), agg.ID().ID, m.appKey)
	tx := txFromCtx(ctx)
	mut := datastore.NewInsert(key, agg)
	_, err := tx.Mutate(mut)
	if err != nil {
		return wraperr.Wrap(err, "error creating aggregate")
	}
	return nil
}

func (m *Repo) CommitEvent(ctx context.Context, event ocqrs.Event) error {
	aggKey := datastore.NameKey(ocqrs.TypeOf(event.Agg()), event.Agg().ID().ID, m.appKey)
	eventKey := datastore.IDKey("event", event.Version(), aggKey)
	tx := txFromCtx(ctx)
	encode, err := json.Marshal(event)
	if err != nil {
		return wraperr.Wrap(err, "error encoding")
	}
	_ = encode
	record := &eventRecord{
		Type: ocqrs.TypeOf(event),
		Data: event,
	}
	mut := datastore.NewInsert(eventKey, record)
	_, err = tx.Mutate(mut)
	if err != nil {
		return wraperr.Wrap(err, "error creating event")
	}
	return nil
}

func (m *eventRecord) Load(prop []datastore.Property) error {
	var data *datastore.Entity
	for _, p := range prop {
		switch p.Name {
		case "Type":
			str, ok := p.Value.(string)
			if !ok {
				return errors.New("error type must be a string")
			}
			m.Type = str
		case "Data":
			ent, ok := p.Value.(*datastore.Entity)
			if !ok {
				return errors.New("error data must be a entity")
			}
			data = ent
		}
	}
	if data == nil {
		return errors.New("Need data")
	}
	m.dataCache = data.Properties
	return nil
}

func (m *eventRecord) Save() ([]datastore.Property, error) {
	return datastore.SaveStruct(m)
}
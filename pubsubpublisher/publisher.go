package pubsubpublisher

import (
	"cloud.google.com/go/pubsub"
	"context"
	"encoding/json"
	"github.com/ozouai/ocqrs"
	"github.com/ozouai/ocqrs/wraperr"
)

type Publisher struct {
	client *pubsub.Client
	topicName string
	topic *pubsub.Topic
}

func New(projectID string, topic string) (*Publisher, error) {
	c, err := pubsub.NewClient(context.TODO(), projectID)
	if err != nil {
		return nil, wraperr.Wrap(err, "error creating pubsub client")
	}
	t := c.Topic(topic)
	return &Publisher{client: c,
		topicName: topic,
	topic:t,
	}, nil
}

func (m *Publisher) NotifyAgg(ctx context.Context, notif ocqrs.AggregateNotification) error {
	data, err := json.Marshal(notif)
	if err != nil {
		return wraperr.Wrap(err, "error marshaling aggnotif")
	}
	res := m.topic.Publish(ctx, &pubsub.Message{
		Data: data,
	})
	_, err = res.Get(ctx)
	if err != nil {
		return wraperr.Wrap(err, "error publishing message")
	}
	return nil
}
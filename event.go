package ocqrs


type EventBase struct {
	agg Aggregate
	version int64
}

type Event interface {
	Agg() Aggregate
	Version() int64

	base() *EventBase
}

func NewEventBase(agg Aggregate) *EventBase {
	return &EventBase{agg: agg}
}

func (m *EventBase) Agg() Aggregate {
	return m.agg
}

func (m *EventBase) Version() int64 {
	return m.version
}

func (m *EventBase) base() *EventBase {
	return m
}

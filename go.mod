module github.com/ozouai/ocqrs

go 1.13

require (
	cloud.google.com/go/datastore v1.1.0
	cloud.google.com/go/pubsub v1.2.0
	google.golang.org/grpc v1.27.1
)

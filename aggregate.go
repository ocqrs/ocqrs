package ocqrs

import (
	"context"
	"github.com/ozouai/ocqrs/wraperr"
)

type AggregateID struct {
	ID string
}

type AggregateBase struct {
	id AggregateID
	version int64
	changes []Event
}

type Aggregate interface {
	EventFactories() []func(b *EventBase)Event
	ID() AggregateID
	Apply(Event)

	base() *AggregateBase
	IncrementVersion()
}

func (m AggregateBase) ID() AggregateID {
	return m.id
}

func (m *AggregateBase) base() *AggregateBase {
	return m
}

func NewAggregateBase(id AggregateID) *AggregateBase {
	return &AggregateBase{id: id}
}

func TrackChange(agg Aggregate, ev Event) {
	m := agg.base()
	m.changes = append(m.changes, ev)
	ev.base().version = m.version+int64(len(m.changes))
	agg.Apply(ev)
}

type Repo interface {
	CommitEvent(ctx context.Context, event Event) error
}

type AggregateNotification struct {
	Type string `json:"type"`
	ID string `json:"id"`
	Version int64 `json:"version"`
}

type Publisher interface {
	NotifyAgg(ctx context.Context, notif AggregateNotification) error
}

func CommitAggregate(ctx context.Context, repo Repo, publisher Publisher, agg Aggregate) error {
	for _, change := range agg.base().changes {
		err := repo.CommitEvent(ctx, change)
		if err != nil {
			return wraperr.Wrap(err, "error commiting change")
		}
	}
	agg.base().version+=int64(len(agg.base().changes))
	agg.base().changes = []Event{}
	err := publisher.NotifyAgg(ctx, AggregateNotification{Type:TypeOf(agg), ID:agg.ID().ID, Version:agg.base().version})
	if err != nil {
		return wraperr.Wrap(err, "error sending aggregate notification")
	}
	return nil
}

func (m *AggregateBase) IncrementVersion() {
	m.version++
}
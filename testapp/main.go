package main

import (
	"context"
	"github.com/ozouai/ocqrs"
	"github.com/ozouai/ocqrs/datastorerepo"
	"github.com/ozouai/ocqrs/wraperr"
	"github.com/ozouai/ocqrs/pubsubpublisher"
	"log"
)

func main() {
	repo, err := datastorerepo.NewRepo("ocqrs-test", "test", []ocqrs.Aggregate{
		&TestAgg{},
	})

	if err != nil {
		panic(err)
	}
	publisher, err := pubsubpublisher.New("ocqrs-test", "cqrs-test")
	if err != nil {
		panic(err)
	}
	err = repo.WithTx(context.TODO(), func(ctx context.Context) error {
		agg := &TestAgg{AggregateBase: ocqrs.NewAggregateBase(ocqrs.AggregateID{ID: "bruh"})}
		err := repo.CreateAgg(ctx, agg)
		if err != nil {
			return wraperr.Wrap(err, "error creating agg")
		}
		agg.MakePersonOfInterest()
		err = ocqrs.CommitAggregate(ctx, repo, publisher, agg)
		if err != nil {
			return wraperr.Wrap(err, "error committing ev")
		}

		//agg2 := &TestAgg{AggregateBase: ocqrs.NewAggregateBase(ocqrs.AggregateID{ID: "bruh"})}
		//exists, err := repo.AggExists(ctx, agg2)
		//if err != nil {
		//	return wraperr.Wrap(err, "error checking exists")
		//}
		//if !exists {
		//	panic("not exist")
		//}
		//err = repo.FetchAgg(ctx, agg2)
		//if err != nil {
		//	return wraperr.Wrap(err, "error committing ev")
		//}
		//agg2.MakePersonOfInterest()
		//err = ocqrs.CommitAggregate(ctx, repo, publisher, agg2)
		//if err != nil {
		//	return wraperr.Wrap(err, "error committing ev")
		//}
		return nil
	})
	if err != nil {
		panic(err)
	}
}

type TestAgg struct {
	*ocqrs.AggregateBase
}

type TestEvent struct {
	*ocqrs.EventBase
	Person string
}

func (t *TestAgg) EventFactories() []func(b *ocqrs.EventBase) ocqrs.Event {
	return []func(b *ocqrs.EventBase) ocqrs.Event {
		func(b *ocqrs.EventBase) ocqrs.Event{
			return &TestEvent{EventBase: b}
		},
	}
}

func (m *TestAgg) Apply(ev ocqrs.Event) {
	switch e := ev.(type) {
	case *TestEvent:
		log.Println("Got Person:"+e.Person)
	}
}

func (m *TestAgg) MakePersonOfInterest() {
	ocqrs.TrackChange(m, &TestEvent{
		EventBase: ocqrs.NewEventBase(m),
		Person: "Of Interest",
	})
}
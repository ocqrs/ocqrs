package ocqrs

import "reflect"

func TypeOf(t interface{}) string {
	return reflect.TypeOf(t).Elem().Name()
}